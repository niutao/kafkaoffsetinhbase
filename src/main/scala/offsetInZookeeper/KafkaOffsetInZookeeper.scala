package offsetInZookeeper

import kafka.utils.{ZKGroupTopicDirs, ZkUtils}
import org.I0Itec.zkclient.ZkClient
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.common.TopicPartition
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.streaming.kafka010.ConsumerStrategies.{Assign, Subscribe}
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.kafka010.{ConsumerStrategies, HasOffsetRanges, KafkaUtils, OffsetRange}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.zookeeper.ZooDefs
import org.apache.zookeeper.data.ACL

import scala.collection.JavaConversions
import scala.collection.mutable.ListBuffer

/**
  * Created by angel
  */
object KafkaOffsetInZookeeper {
  def main(args: Array[String]): Unit = {
    //5 cdh1:9092,cdh2:9092,cdh3:9092 test2 zk cdh1:2181,cdh2:2181,cdh3:2181
    if (args.length < 5) {
      System.err.println("Usage: KafkaDirectStreamTest " +
        "<batch-duration-in-seconds> " +
        "<kafka-bootstrap-servers> " +
        "<kafka-topics> " +
        "<kafka-consumer-group-id> " +
        "<kafka-zookeeper-quorum>")
      System.exit(1)
    }

    val batchDuration = args(0)
    val bootstrapServers = args(1).toString
    val topicsSet = args(2).toString.split(",").toSet
    val consumerGroupID = args(3)
    val zkQuorum = args(4)
    val sparkConf = new SparkConf().setAppName("Kafka-Offset-Management-Blog")
      .setMaster("local[4]")//Uncomment this line to test while developing on a workstation
    val sc = new SparkContext(sparkConf)
    val ssc = new StreamingContext(sc, Seconds(batchDuration.toLong))
    val topics = topicsSet.toArray
    val topic = topics(0)
    //  /consumers/[groupId]/offsets/topic/[partitionId]
    //+"/consumers/"+consumerGroupID+"/offsets/"+topic
    val zkKafkaRootDir = zkQuorum + "/consumers/"+consumerGroupID+"/offsets/"+topic
    val zkSessionTimeOut = 10000
    val zkConnectionTimeOut = 10000
    val zkClientAndConnection = ZkUtils.createZkClientAndConnection(zkKafkaRootDir, zkSessionTimeOut, zkConnectionTimeOut)
    val zkUtils = new ZkUtils(zkClientAndConnection._1, zkClientAndConnection._2, false)


    val kafkaParams = Map[String, Object](
      "bootstrap.servers" -> bootstrapServers,
      "key.deserializer" -> classOf[StringDeserializer],
      "value.deserializer" -> classOf[StringDeserializer],
      "group.id" -> consumerGroupID,
      "auto.offset.reset" -> "latest",
      "enable.auto.commit" -> (false: java.lang.Boolean)
    )

    //去zookeeper上拿offset
    val fromOffsets: Map[TopicPartition, Long] = readOffsets(topics , consumerGroupID , zkUtils)
    //根据offset获取数据
//    val inputDStream = KafkaUtils.createDirectStream[String, String](
//      ssc,
//      PreferConsistent,
//      Assign[String, String](fromOffsets.keys,kafkaParams,fromOffsets)
//    )

    //offsets: ju.Map[TopicPartition, jl.Long]
//    val inputDStream = KafkaUtils.createDirectStream[String, String](
//      ssc,
//      PreferConsistent,
//      Subscribe[String, String](topics, kafkaParams , fromOffsets)
//    )
    val inputDStream = KafkaUtils.createDirectStream(ssc, PreferConsistent, ConsumerStrategies.Subscribe[String,String](topics, kafkaParams, fromOffsets))
    //处理数据，处理完事之后将offset写入zookeeper
    var storeEndOffset: Boolean = false
    inputDStream.foreachRDD((rdd,batchTime) => {

      val offsetRanges = rdd.asInstanceOf[HasOffsetRanges].offsetRanges
      offsetRanges.foreach(
        offset =>
          println(offset.topic, offset.partition, offset.fromOffset,offset.untilOffset)
      )
      val newRDD = rdd.map(message => processMessage(message))
//      newRDD.count()
      persistOffsets(offsetRanges,consumerGroupID,storeEndOffset,zkUtils)
    })

//    println("Number of messages processed " + inputDStream.count())
    ssc.start()
    ssc.awaitTermination()



  }

  /*
    Create a dummy process that simply returns the message as is.
     */
  def processMessage(message:ConsumerRecord[String,String]):ConsumerRecord[String,String]={
    message
  }

  def readOffsets(
                   topics: Seq[String],
                   groupId:String ,
                   zkUtils: ZkUtils
                 ): Map[TopicPartition, Long] = {

    val topicPartOffsetMap = collection.mutable.HashMap.empty[TopicPartition, Long]

    val partitionMap = zkUtils.getPartitionsForTopics(topics)

    // /consumers/<groupId>/offsets/<topic>/

    partitionMap.foreach(topicPartitions => {

      val zkGroupTopicDirs = new ZKGroupTopicDirs(groupId, topicPartitions._1)
      //遍历每一个分区下的数据
      topicPartitions._2.foreach(partition => {

        val offsetPath = zkGroupTopicDirs.consumerOffsetDir + "/" + partition
        try {

          val offsetStatTuple = zkUtils.readData(offsetPath)
          if (offsetStatTuple != null) {
            topicPartOffsetMap.put(new TopicPartition(topicPartitions._1, Integer.valueOf(partition)), offsetStatTuple._1.toLong)

          }

        } catch {

          case e: Exception =>

//            println("retrieving offset details - no previous node exists:" + " {}, topic: {}, partition: {}, node path: {}", Seq[AnyRef](e.getMessage, topicPartitions._1, partition.toString, offsetPath): _*)
            println("message: {} , topic: {}, partition: {},  node path: {}" , e.getMessage , topics , topicPartitions ,  offsetPath)
            topicPartOffsetMap.put(new TopicPartition(topicPartitions._1, Integer.valueOf(partition)), 0L)

        }

      })

    })

    topicPartOffsetMap.toMap

  }


  def persistOffsets(
                      offsets: Seq[OffsetRange],
                      groupId: String,
                      storeEndOffset: Boolean = true,
                      zkUtils: ZkUtils
                    ): Unit = {


    offsets.foreach(or => {
      val zkGroupTopicDirs = new ZKGroupTopicDirs(groupId, or.topic);
      val offsetPath = zkGroupTopicDirs.consumerOffsetDir + "/" + or.partition;
      val offsetVal = if (storeEndOffset) or.untilOffset else or.fromOffset
      println(or.topic.toString , or.partition.toString , offsetVal , offsetPath)
      zkUtils.updatePersistentPath(zkGroupTopicDirs.consumerOffsetDir + "/" + or.partition, offsetVal + "")//, JavaConversions.bufferAsJavaList(acls)

    })

  }


}
